package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	id64,_ := strconv.Atoi(os.Args[1])
	bits32 := 76561197960265728

	id32 := (id64 - bits32) / 2

	id32string := strconv.Itoa(id32)

	fmt.Println("STEAM_0:1:"+ id32string)
}
