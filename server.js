"use strict"

const express = require("express")
const url = require("url")
const users = require("./models/steam-users.js")
const bodyParser = require("body-parser")
const app = express()

app.use(bodyParser.urlencoded({extended: true}))
app.use(express.static("public"))
app.set("views", process.cwd() + "/templates")
app.set("view engine", "jade")

app.get("/", (req,res)=>{
	res.render("index")
})

app.post("/user", (req,res)=>{
	if(req.body.nickname)
		users.getUserCard(res, {nickname: req.body.nickname})
	else
		res.redirect('back')
})

app.get("/user", (req,res)=>{
	let query = url.parse(req.url, true).query
	users.getUserCard(res, query)
})

app.listen(process.env.PORT || 8080)
