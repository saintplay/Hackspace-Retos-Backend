# Steam Id Finder in Heroku
Este ejercicio puede ser probado en : https://steamidfinder.herokuapp.com/

También se pueden usar querys, ejemplos! :
- https://steamidfinder.herokuapp.com/user?nickname=saintplay96
- https://steamidfinder.herokuapp.com/user?id=76561198194816999

## Modulos usados:
- **express** : Para armar el servidor y el router.
- **body-parser"** : Para detectar el nickname que el usuario nos está dando.
- **url** : Para otener el query, en caso de que lo soliciten.
- **jade** : Para el server side templating
- **sync-request**: Para hacer la petición a el API de Steam de manera síncrona.
- **child_process** : Para ejecutar binarios externos(steamid.go).
- **steamid.go** : Para convertir el id de 64 bits a 32 bits.

### API Keys para testing(Cortesía de [Brayan Cruces](https://github.com/brayancruces) y mía)
- 99F44A562B42CD25E33699FEBF407773
- 21A3C1A66F9286E18B14AA8FDA38CA7E
- 2BB87343D12D95A9080B7D3DACE9EFA2

## Otros Retos:
- Reto 1: https://gitlab.com/saintplay96/Hackspace-Retos-Backend/tree/master 
- Reto 3: https://gitlab.com/saintplay96/Hackspace-Retos-Backend/tree/reto3
