"use strict"

const request = require("sync-request")
const spawn = require("child_process").spawnSync

const steamKey = "99F44A562B42CD25E33699FEBF407773"

const hostname = "http://api.steampowered.com"

const requestIdUrl = "/ISteamUser/ResolveVanityURL/v0001/?key=" + steamKey + "&vanityurl="
const requestUserUrl = "/ISteamUser/GetPlayerSummaries/v0002/?key=" + steamKey +"&steamids="

const apiIdURL = hostname + requestIdUrl
const apiUserUrl = hostname + requestUserUrl


function getIdJSON(nickname){

	let response = request("GET", apiIdURL + nickname)
	response = JSON.parse(response.getBody())

	return response.response
}

function getUserJSON(id){
	let response = request("GET", apiUserUrl + id)
	response = JSON.parse(response.getBody())

	if(response.response.players.length)
		return response.response.players[0]
	else
		return 0
}

const Users = (()=>{
	return {
		getUserCard : (serverResponse, query)=>{
			
			let id
			let id64
			let user
			let founded = false

			if(query.nickname){

				let userIdResponse = getIdJSON(query.nickname)
				
				if(userIdResponse.success === 1){
					founded = true
					id64 = userIdResponse.steamid
					user = getUserJSON(id64)
					
					id = spawn("./steamid", [id64]).stdout.toString()
				}
			}
			else if(query.id){
				user = getUserJSON(query.id)
				if(user) {
					founded = true
					id = spawn("./steamid", [query.id]).stdout.toString()
				}
			}

			if(founded) {
				user.id = id
				serverResponse.render("user", user)
			}
			else
				serverResponse.render("index", query)
		}
	}
})()

module.exports = Users
